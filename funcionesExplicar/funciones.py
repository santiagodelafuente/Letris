#!/usr/bin/env python3

from configuracion import *
from principal import *
import math
import random


#------------------------------------------------------------------------


def lectura(listaPalabras):
    nombres = open("nombresOriginal.txt")
    for n in nombres:
        listaPalabras.append(n.rstrip("\n"))


#------------------------------------------------------------------------


def puntuar(candidata):
    vocales="aeiou"
    dificiles="jkqwxyz"
    puntos=0
    for letra in candidata:
        if letra in vocales:
            puntos+=1
        elif letra in dificiles:
            puntos+=5
        else:
            puntos+=2
    return puntos


#------------------------------------------------------------------------


def moverLetra(posicion, direccion):
    if direccion == "right" and posicion[0] < ANCHO-1.5*TAMANO_LETRA:
        posicion[0]+=TAMANO_LETRA
    if direccion == "left" and posicion[0] > 5:
        posicion[0]-=TAMANO_LETRA


#------------------------------------------------------------------------


def nuevaLetra():
    vocal="aeiou"
    faciles="bcdfghlmnprstv"
    dificiles="jkqwxyz"
    n=random.randint(1,11)
    if n<=5:
        return random.choice(vocal)
    elif n<=9:
        return random.choice(faciles)
    else:
        return random.choice(dificiles)


#------------------------------------------------------------------------


def nuevaPosicion(posicion):
    x=random.randrange(TAMANO_LETRA,ANCHO-TAMANO_LETRA,TAMANO_LETRA)
    y=TAMANO_LETRA+5
    posicion[0]=x
    posicion[1]=y


#------------------------------------------------------------------------


def posicionLibre(posX, posY, ocupadosX, ocupadosY):
    for i in range(len(ocupadosX)):
        if abs(posX-ocupadosX[i])<TAMANO_LETRA:
            if abs(posY-ocupadosY[i])<TAMANO_LETRA:
                return False
    return True


#------------------------------------------------------------------------


def dameIndicesDeUnaFila(numeroDeFila, ocupadosY):
    listaIndices=[]
    for i in range(len(ocupadosY)):
        if ocupadosY[i] == numeroDeFila:
            listaIndices.append(i)
    print(listaIndices)
    return listaIndices


#------------------------------------------------------------------------


def dameElementosLista(indices, lista):
    indices = listaIndices
    lista = letrasEnPantalla


#------------------------------------------------------------------------


def ordenarLetras(listaLetras, lista):
    pass


#------------------------------------------------------------------------


def armarPalabra(letrasEnPantalla, ocupadosX, ocupadosY):
    fila=ocupadosY[len(ocupadosY)-1]
    indices=[]
    for i in range(len(ocupadosY)):
        if ocupadosY[i]==fila:
            indices.append(i)
   letrasEnFila=[]
   for indice in indices:
       letrasEnFila.append(letrasEnPantalla[indice])

   losXenFila=[]
   for indice in indices:
       losXenFila.append(ocupadosX[indice])

   copiaX=losXenFila[:]
   copiaX.sort()


#------------------------------------------------------------------------


def actualizar(letra, posicion, letrasEnPantalla, ocupadosX, ocupadosY):
    if posicion[1] <= (ALTO - 100) and posicionLibre(posicion[0], posicion[1], ocupadosX, ocupadosY):
        posicion[1] = posicion[1] + 1.5
        return False
    else:
        letrasEnPantalla.append(letra)
        ocupadosX.append(posicion[0])
        ocupadosY.append(posicion[1])
        if musicaSonando:
            sonidoCaeLetra.play()
        return True



#------------------------------------------------------------------------


def main():
    pass



#------------------------------------------------------------------------


if __name__ == "__main__":
    main()
