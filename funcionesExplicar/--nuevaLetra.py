def nuevaLetra():
    vocal="aeiou"
    faciles="bcdfghlmnprstv"
    dificiles="jkqwxyz"
    n=random.randint(1,11)
    if n<=5:
        return random.choice(vocal)
    elif n<=9:
        return random.choice(faciles)
    else:
        return random.choice(dificiles)