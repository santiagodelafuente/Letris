def puntuar(candidata):
    vocales="aeiou"
    dificiles="jkqwxyz"
    puntos=0
    for letra in candidata:
        if letra in vocales:
            puntos+=1
        elif letra in dificiles:
            puntos+=5
        else:
            puntos+=2
    return puntos