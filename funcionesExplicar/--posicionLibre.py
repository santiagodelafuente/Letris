def posicionLibre(posX, posY, ocupadosX, ocupadosY):
    for i in range(len(ocupadosX)):
        if abs(posX-ocupadosX[i])<TAMAÑO_LETRA_GRANDE:
            if abs(posY-ocupadosY[i])<TAMAÑO_LETRA_GRANDE:
                return False
    return True
