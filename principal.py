#!/usr/bin/env python3

import math, os, random, sys

import pygame
from pygame.locals import *

from configuracion import *
from funciones import *
from extras import *


def main():
    os.environ["SDL_VIDEO_CENTERED"] = "1"
    pygame.init()
    # pygame.mixer.init()

    # preparar la ventana
    pygame.display.set_caption("LETRIS 2017")
    screen = pygame.display.set_mode((ANCHO, ALTO))
   
    ventana = pygame.display.get_surface()
    superficie_imagen = pygame.image.load("images/fondo1.jpg")
    ventana.blit(superficie_imagen, (-200,50))
    pygame.display.flip() 
    

    # Tiempo total del juego
    gameClock = pygame.time.Clock()
    totaltime = 0
    segundos = TIEMPO_MAX
    fps = FPS_INICIAL

    puntos = 0
    candidata = ""

    diccionario = []
    lectura(diccionario)
    letra = nuevaLetra()
    proxLetra = nuevaLetra()
    posicion = [0, 0]
    nuevaPosicion(posicion)

    ocupadosX = []
    ocupadosY = []
    letrasEnPantalla = []

    while segundos > fps / 1000:
        # 1 frame cada 1/fps segundos
        gameClock.tick(fps)
        totaltime += gameClock.get_time()

        fps = 60

        for e in pygame.event.get():
            if e.type == QUIT:
                pygame.quit()
                return
            
            if e.type == KEYDOWN:
                direccion = dameTeclaApretada(e.key)
                moverLetra(posicion, direccion)

        segundos = TIEMPO_MAX - pygame.time.get_ticks() / 1000

        bandera = actualizar(letra, posicion, letrasEnPantalla, ocupadosX, ocupadosY)

        if bandera:
            letra = proxLetra
            proxLetra =nuevaLetra()
            nuevaPosicion(posicion)
            candidata = armarPalabra(letrasEnPantalla, ocupadosX, ocupadosY)
            print(candidata)
            if candidata in diccionario:
                puntos += puntuar(candidata)

        # limpiar pantalla anterior
        screen.fill(COLOR_FONDO)
        ventana.blit(superficie_imagen, (-200,50))
        
        dibujar(screen, letra, proxLetra, posicion, letrasEnPantalla, ocupadosX, ocupadosY, puntos, segundos)
        pygame.display.flip()

    while True:
        for e in pygame.event.get():
            if e.type == QUIT:
                pygame.quit()
                return


if __name__ == "__main__":
    main()
