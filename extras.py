#!/usr/bin/env python3

import pygame
from pygame.locals import *
from configuracion import *

pygame.mixer.init()
pygame.mixer.music.load("music/sound1.mp3")
sonidoCaeLetra = pygame.mixer.Sound('music/caeLetra.wav')
pygame.mixer.music.play(-1)
musicaSonando = True

def dameTeclaApretada(key):
    if key == K_UP or key == K_k:
        return "up"
    elif key == K_DOWN or key == K_j:
        return "down"
    elif key == K_RIGHT or key == K_l:
        return "right"
    elif key == K_LEFT or key == K_h:
        return "left"
    else:
        return ""


def dibujar(screen, letra, proxLetra, posicion, letrasEnPantalla, ocupadosX, ocupadosY, puntos, segundos):
    defaultFont = pygame.font.Font(pygame.font.get_default_font(), TAMANO_LETRA)
    defaultFontGRANDE = pygame.font.Font(pygame.font.get_default_font(), TAMANO_LETRA_GRANDE)

    pygame.draw.line(screen, (255, 255, 255), (0, ALTO - 70), (ANCHO, ALTO - 70), 5)

    ren1 = defaultFont.render("Puntos: " + str(puntos), 1, COLOR_TEXTO)
    ren2 = defaultFont.render("Tiempo: " + str(int(segundos)), 1, COLOR_TIEMPO_FINAL if segundos < 15 else COLOR_TEXTO)
    ren3 = defaultFontGRANDE.render(letra, 1, COLOR_LETRA)
    ren4 = defaultFont.render("Proxima Letra: " + str(proxLetra), 1, COLOR_TEXTO)

    i = 0
    while i < len(letrasEnPantalla):
        screen.blit(defaultFontGRANDE.render(letrasEnPantalla[i], 1, COLOR_TEXTO), (ocupadosX[i], ocupadosY[i]))
        i += 1

    screen.blit(ren1, (ANCHO - 120, 10))
    screen.blit(ren2, (10, 10))
    screen.blit(ren3, (posicion[0], posicion[1]))
    screen.blit(ren4, (ANCHO - 170, ALTO - 20))