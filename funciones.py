#!/usr/bin/env python3

from configuracion import *
from principal import *
import math
import random


# Cargar en listaPalabras las palabras del archivo.
def lectura(listaPalabras):
    nombres = open("nombresOriginal.txt")
    for n in nombres:
        listaPalabras.append(n.rstrip("\n").lower())


# Puntua la palabra.
# Okay
def puntuar(candidata):
    vocales="aeiou"
    dificiles="jkqwxyz"
    puntos=0
    for letra in candidata:
        if letra in vocales:
            puntos+=1
        elif letra in dificiles:
            puntos+=5
        else:
            puntos+=2
    return puntos

def moverLetra(posicion, direccion):
    if direccion == "right" and posicion[0] < ANCHO-1.5*TAMANO_LETRA:
        posicion[0]+=TAMANO_LETRA
    if direccion == "left" and posicion[0] > 5:
        posicion[0]-=TAMANO_LETRA

# Devuelve una letra del abecedario al azar.
def nuevaLetra():
    #vocal="aeiou"
    #faciles="bcdfghlmnprstv"
    #dificiles="jkqwxyz"
    #n=random.randint(1,11)
    #if n<=5:
        #return random.choice(vocal)
    #elif n<=9:
        #return random.choice(faciles)
    #else:
        #return random.choice(dificiles)
    return random.choice("an")

def nuevaPosicion(posicion):
    x=random.randrange(TAMANO_LETRA,ANCHO-TAMANO_LETRA,TAMANO_LETRA)
    y=TAMANO_LETRA+5
    posicion[0]=x
    posicion[1]=y


def posicionLibre(posX, posY, ocupadosX, ocupadosY):
    for i in range(len(ocupadosX)):
        if abs(posX-ocupadosX[i])<TAMANO_LETRA:
            if abs(posY-ocupadosY[i])<TAMANO_LETRA:
                return False
    return True


# Opcional
def dameIndicesDeUnaFila(numeroDeFila, ocupadosY):
    listaIndices=[]
    for i in range(len(ocupadosY)):
        if ocupadosY[i] == numeroDeFila:
            listaIndices.append(i)
    # print(listaIndices)
    return listaIndices


# Opcional
def dameElementosLista(indices, lista):
    elementos = []
    for i in indices:
        elementos.append(lista[i])
    return elementos

  
def dondeEsta(a,lista):
    for i in range(len(lista)):
        if a==lista[i]:
            return i
        
# Opcional
def ordenarLetras(listaLetras, lista):
    pass

def armarPalabra(letrasEnPantalla, ocupadosX, ocupadosY):
    fila=ocupadosY[len(ocupadosY)-1]

    indices=dameIndicesDeUnaFila(fila,ocupadosY)
    
    letrasEnFila=[]
    for e in indices:
        letrasEnFila.append(letrasEnPantalla[e])
    
    losXenFila=[]
    for o in indices:
        losXenFila.append(ocupadosX[o])

    copiaX=losXenFila[:]
    copiaX.sort()
    
    palabra=""
    for elem in copiaX:
        palabra=palabra+letrasEnFila[dondeEsta(elem,losXenFila)]
    return palabra
        

def actualizar(letra, posicion, letrasEnPantalla, ocupadosX, ocupadosY):
    if posicion[1] <= (ALTO - 100) and posicionLibre(posicion[0], posicion[1], ocupadosX, ocupadosY):
        posicion[1] = posicion[1] + 1.5
        return False
    else:
        letrasEnPantalla.append(letra)
        ocupadosX.append(posicion[0])
        ocupadosY.append(posicion[1])
        if musicaSonando:
            sonidoCaeLetra.play()
        return True

# Opcional
def main():
    pass


if __name__ == "__main__":
    main()
